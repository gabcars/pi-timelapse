#!/usr/bin/env bash

SRCDIR=/home/pi/pi-timelapse

sudo cp -R $SRCDIR/app/ /usr/src/app
sudo cp -R $SRCDIR/flask /usr/src/flask
sudo cp -R $SRCDIR/nginx/default /etc/nginx/sites-available/

# cp static web resources
sudo cp -R $SRCDIR/nginx/html/timelapse.html /var/www/html/
sudo cp -R $SRCDIR/nginx/html/static /var/www/html/static

# cp assets
sudo cp -R $SRCDIR/nginx/html/assets /var/www/html/
