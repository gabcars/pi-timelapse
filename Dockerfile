FROM raspbian/stretch

RUN apt-get update
RUN apt-get install -y  nginx
RUN apt-get install -y  python3-pip
RUN pip3 install flask uwsgi

# on the real pi only: add this
# sudo apt-get install python3-picamera

RUN apt-get install -y vim

COPY ./app/ /usr/src/app
COPY ./flask /usr/src/flask
COPY ./nginx/default /etc/nginx/sites-available/

# copy static web resources
COPY ./nginx/html/timelapse.html /var/www/html/
COPY ./nginx/html/static /var/www/html/static

# forward request and error logs to docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
	&& ln -sf /dev/stderr /var/log/nginx/error.log

# fix: [alert] 1#1: worker process 83084 exited on signal 6
RUN sed -i "s/worker_processes auto;/worker_processes 1;/g" /etc/nginx/nginx.conf

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]

