from picamera import PiCamera
from time import sleep
import os
import pathlib
from datetime import datetime

camera = PiCamera()

#camera.start_preview()

while True:
    sleep(10)
    file = pathlib.Path('/tmp/timelapse.on')
    if file.exists ():
      print('take picture...')
      i = datetime.now()
      camera.capture('/var/www/html/images/image-%s.jpg' % i)
    else:
      print('no take picture')
