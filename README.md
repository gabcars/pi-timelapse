# DEV running a raspbian docker image with nginx and flask

**Prerequisites:**. build the image: `docker build -t pi-timelapse ./`


1. run the docker image `docker run --rm -p 80:80 -p 8000:8000 -d -v /Users/gabriel.damour/PhpstormProjects/pi-timelapse/nginx/html:/var/www/html:ro -v /Users/gabriel.damour/PhpstormProjects/pi-timelapse/app:/usr/src/app:ro pi-timelapse`
2. ssh into it: `docker exec -it <CONTAINER_HASH> bash`
3. Start the Web Server Gateway Interface (uWSGI): `cd /usr/src/flask/; uwsgi --ini uwsgi.ini`
4. Browse on `http://localhost/timelapse.html`

source: https://www.raspberrypi-spy.co.uk/2018/12/running-flask-under-nginx-raspberry-pi/

**On the actual Pi, todo after every restart**

* run the uwsgi (root): `cd /usr/src/flask; uwsgi --ini uwsgi.ini`
* run the timelapse daemon (pi): `cd ~; python3 ./timelapse-daemon.py`

# NEXT

* build a JSON service with flask: https://blog.miguelgrinberg.com/post/designing-a-restful-api-with-python-and-flask
